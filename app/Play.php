<?php

namespace App;


class Play 
{	//this will be the sequence entered by the user
	public $sequence = [null, null, null, null];
	//this will be the representation of the status where 1 is a color mactch and 2 a full match
	public $status = [null, null, null, null];
	public $result = false;
	public $time = null;

	public function setValid(){
		//to set valid will find the first item in $status that is null and we set to 2
		foreach ($this->status as $index => $status) 
		{
			if(!isset($status))
			{
				$this->status[$index] = 2;
				return;
			}

		}
	}

	public function setHasColor(){
	//to set valid will find the first item in $status that is null and we set to 1
		foreach ($this->status as $index => $status) 
		{
			if(!isset($status))
			{
				$this->status[$index] = 1;
				return;
			}
		}
	}
	
	public function check(){
		//will loop to all status itens and find is value is 2, then if we have 4 match we have a winner
		$count = 0;
		foreach ($this->status as $status) 
		{
			if($status == 2)
				$count++;
		}

		if($count == 4)
			$this->result = true;

	}
}

