<?php

namespace App;

use App\Play;
use Carbon\Carbon;


class Game 
{
	public $colors= ['blue', 'rose', 'yellow', 'green', 'orange', 'red'];

	public $start;

	public $player = 'PLAYER';

	protected $sequence =[];

	protected $board =[];

	public $trys = 0;

	public function __construct($name)
	{
		if(strlen($name) > 0)
			$this->player = $name;

		$this->start = new Carbon();

		$this->start();
	}

	public function start(){

		for($i=0; $i < 4; $i++)
		{
			$colorIndex = rand(0, 5);
			$color = $this->colors[$colorIndex];
			array_push($this->sequence, $color);
		}

		for($i=0; $i < 10; $i++)
			array_push($this->board, new Play);

	}

	public function play($sequence){

    	//increment try
		$this->trys++;

    	//get the index where play sequence will be stored
		$index= count($this->board) - $this->trys;

    	//store the sequence, and the board row contains the sequence and status
		$this->board[$index]->sequence = $sequence;

		//set the time of the play
		$this->board[$index]->time = new Carbon();

    	//store the current board item
		$boardRow =$this->board[$index];

		//check how many colors are correct
		$boardRow = $this->validateColors($boardRow);

		return $boardRow;

	}

	public function validateColors(Play $boardRow)
	{
		//create an array for matched colors, so will no validate twice the same color
		$matchedColors =[];
		//also for match indexes
		$indexMatches =[];

		//We loop througth the sequence to find the color and index matchers and we set the status
		foreach ($this->sequence as $index => $color) {

			if($boardRow->sequence[$index] == $color)
			{
				//we save the current index, because when looking at the correct colors we will no consider the ones that are alreay been teste by color and posiion
				array_push($indexMatches, $index);
				$boardRow->setValid();
			}

		}

		//We loop througth the sequence to find the correct colors and exclude the full matches and duplicated colors
		foreach ($boardRow->sequence as $index => $color) {

			if(in_array($color, $this->sequence) && !in_array($color, $matchedColors) && !in_array($index, $indexMatches))
			{	
				array_push($matchedColors, $color);
				$boardRow->setHasColor();		
			}
		}

		//check the result of the boad
		$boardRow->check();

		return $boardRow;


	}


}
