<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <link rel="stylesheet" type="text/css" href="css/app.css">
</head>
<body>
    <div id="app">
        <div class="container hide" v-bind:class="{ 'show' : appStarted }">
            <h3>MASTERMIND</h3>
            <div class="col-xs-12 col-md-offset-4 col-md-4">
                <div class="section" v-if="board.win">
                    <h3>CONGRATS, @{{ name }} YOU WIN THE GAME</h3>
                    <button type="button" @click="board.restart()" class="btn btn-primary">Restart</button>
                </div>
                <div class="section" v-if="board.win == false">
                    <h3>GAME OVER, @{{ name }} YOU LOST THE GAME</h3>
                    <button type="button" @click="board.restart()" class="btn btn-primary">Restart</button>
                </div>
                <div class="section board"  v-if="board.started">
                    <div v-for="(row, rowIndex) in board.rows" class="row">
                    <div class="col-xs-9 sequence">
                            <ul class="colors" v-bind:class="{ 'selected' : board.rowIndex  == rowIndex }">
                                <li v-bind:class="[ (board.row  == rowIndex && board.colorIndex == colorIndex) ? 'selected' : '', color]" @click="board.select(rowIndex, colorIndex)" v-for="(color, colorIndex) in row.sequence">

                                </li>   
                            </ul>
                        </div>   
                        <div class="col-xs-3">
                          <ul class="status">
                              <li v-bind:class="(status == null) ? '' : ((status == 2)  ? 'match' : 'color-match')" v-for="status in row.status"></li>
                          </ul>
                      </div> 
                  </div>
                  
              </div>
              <div class="section" v-if="board.canCheck">
                 <button type="button" @click="board.check()" class="btn btn-primary">Check</button>
             </div>
             <div class="section" v-if="!board.started">
                <input type="text" v-model="name"  class="form-control name" placeholder="Enter you name" required="required" >
                <button type="button" @click="board.start(name)" class="btn btn-primary">START</button>
            </div>
            <div  class="section">
                <ul class="colors">
                    <li class="blue" @click="board.play('blue')"></li>
                    <li class="rose" @click="board.play('rose')"></li>
                    <li class="yellow" @click="board.play('yellow')"></li>
                    <li class="green" @click="board.play('green')"></li>
                    <li class="orange" @click="board.play('orange')"></li>
                    <li class="red" @click="board.play('red')"></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
